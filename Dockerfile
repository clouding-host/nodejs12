# ----------------------------------
#   Clouding-host nodejs12 docker
#    Environement: NodeJS - 12
#   Minimum Panel Version: 0.6.0
# ----------------------------------


FROM debian:10
MAINTAINER Matheo Leduc, <contact@clouding-host.com>
LABEL       author="Matheo Leduc" maintainer="contact@clouding-host.com"

RUN apt update && apt upgrade -y && apt install curl -y && curl -sL https://deb.nodesource.com/setup_12.x | bash && apt install gnupg make gcc g++ nodejs -yq && apt clean -y && useradd -d /home/container -m container

USER container
ENV USER=container HOME=/home/container

WORKDIR /home/container

COPY ./entrypoint.sh /entrypoint.sh
CMD ["/bin/bash", "/entrypoint.sh"]